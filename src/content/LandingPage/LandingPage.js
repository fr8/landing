import React from 'react';
import {
  Breadcrumb,
  BreadcrumbItem,
  Button,
  Tabs,
  Tab,
} from 'carbon-components-react';
import { InfoSection, InfoCard } from '../../components/Info';
import Globe32 from '@carbon/icons-react/lib/globe/32';
import PersonFavorite32 from '@carbon/icons-react/lib/person--favorite/32';
import Application32 from '@carbon/icons-react/lib/application/32';

const props = {
  tabs: {
    selected: 0,
    triggerHref: '#',
    role: 'navigation',
  },
  tab: {
    href: '#',
    role: 'presentation',
    tabIndex: 0,
  },
};

const LandingPage = () => {
  return (
    <div className="bx--grid bx--grid--full-width landing-page">
      <div className="bx--row landing-page__banner">
        <div className="bx--col-lg-16">
          <Breadcrumb noTrailingSlash aria-label="Page navigation">
            <BreadcrumbItem>
              <a href="/">EDI Meets Blockchain</a>
            </BreadcrumbItem>
          </Breadcrumb>
          <h1 className="landing-page__heading">
            EDI Transactions &amp; Smart Contracts
          </h1>
        </div>
      </div>
      <div className="bx--row landing-page__r2">
        <div className="bx--col bx--no-gutter">
          <Tabs {...props.tabs} aria-label="Tab navigation">
            <Tab {...props.tab} label="Maidenlane Protocol">
              <div className="bx--grid bx--grid--no-gutter bx--grid--full-width">
                <div className="bx--row landing-page__tab-content">
                  <div className="bx--col-md-4 bx--col-lg-7">
                    <h2 className="landing-page__subheading">
                      What is Maidenlane?
                    </h2>
                    <p className="landing-page__p">
                      The world as we know it runs on and depends on EDI. 
                      EDI will remain highly useful and widely used for years
                      to come, which is why we built maidenlane. Maidenlane
                      is the name of our smart contract protocol that enables
                      existing EDI transactions to function on our Hyperledger
                      Besu Network. 
                    </p>
                    <Button>Get Started Today</Button>
                  </div>
                  <div className="bx--col-md-4 bx--offset-lg-1 bx--col-lg-8">
                    <img
                      className="landing-page__illo"
                      src={`${process.env.PUBLIC_URL}/tab-illo.png`}
                      alt="Carbon illustration"
                    />
                  </div>
                </div>
              </div>
            </Tab>
            <Tab {...props.tab} label="How it Works">
              <div className="bx--grid bx--grid--no-gutter bx--grid--full-width">
                <div className="bx--row landing-page__tab-content">
                  <div className="bx--col-lg-16">
                    Maidenlane is the next evolution in financial instruments for commodities
                  </div>
                </div>
              </div>
            </Tab>
            <Tab {...props.tab} label="Features">
              <div className="bx--grid bx--grid--no-gutter bx--grid--full-width">
                <div className="bx--row landing-page__tab-content">
                  <div className="bx--col-lg-16">
                    From Defense Supply Chains needing ITAR/DFARS to 
                    Pharama Cold-Chain and everything in between.
                  </div>
                </div>
              </div>
            </Tab>
          </Tabs>
        </div>
      </div>
      <InfoSection heading="The Protocol" className="landing-page__r3">
        <InfoCard
          heading="Maidenlane is Open"
          body="It's a distributed effort, guided by the principles of the open-source movement. Maidenlane's users are also it's makers, and users."
          icon={<PersonFavorite32 />}
        />
        <InfoCard
          heading="Maidenlane is Native EDI"
          body="Maidenlane's modularity ensures maximum compatibility in execution. It's composable protocol parts are designed to work seamlessly with each other, in whichever combination suits the needs of the user."
          icon={<Application32 />}
        />
        <InfoCard
          heading="Maidenlane is Performant"
          body="Built from the ground up using modern langauges like Rust, every element and component of Maidenlane was designed from the ground up to work elegantly together to ensure consistent, performant, and fault tolerance."
          icon={<Globe32 />}
        />
      </InfoSection>
    </div>
  );
};

export default LandingPage;

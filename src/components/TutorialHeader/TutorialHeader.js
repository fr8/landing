import React from 'react';
import {
  Header,
  HeaderName,
  HeaderNavigation,
  HeaderMenuItem,
  HeaderGlobalBar,
  HeaderGlobalAction,
  SkipToContent,
} from 'carbon-components-react/lib/components/UIShell';
import Notification20 from '@carbon/icons-react/lib/notification/20';
import UserAvatar20 from '@carbon/icons-react/lib/user--avatar/20';
import AppSwitcher20 from '@carbon/icons-react/lib/app-switcher/20';
import { Link } from 'react-router-dom';

const TutorialHeader = () => (
  <Header aria-label="Freight Trust">
    <SkipToContent />
    <HeaderName element={Link} to="/" prefix="EDI">
      Freight Trust
    </HeaderName>
    <HeaderNavigation aria-label="Freight Trust">
      <HeaderMenuItem element={Link} to="/repos">
        Trucking     
      </HeaderMenuItem>
            <HeaderMenuItem element={Link} to="/repos">
        Maritime     
      </HeaderMenuItem>
            <HeaderMenuItem element={Link} to="/repos">
        Freight Forwarder     
      </HeaderMenuItem>
            <HeaderMenuItem element={Link} to="/repos">
        Customs & Defense      
      </HeaderMenuItem>
    </HeaderNavigation>
    <HeaderGlobalBar>
      <HeaderGlobalAction aria-label="Notifications">
        <Notification20 />
      </HeaderGlobalAction>
      <HeaderGlobalAction aria-label="User Avatar">
        <UserAvatar20 />
      </HeaderGlobalAction>
      <HeaderGlobalAction aria-label="App Switcher">
        <AppSwitcher20 />
      </HeaderGlobalAction>
    </HeaderGlobalBar>
  </Header>
);

export default TutorialHeader;
